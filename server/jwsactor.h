// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#ifndef JWSACTOR_H
#define JWSACTOR_H

#include <string>

class jwsactor
{
private:
  static const unsigned int buffsz= 2048;
  
  std::string buffer;  
  int fd;
  unsigned int last_event;
  
  void handshake();  

public:
  
  explicit jwsactor(const int);
  explicit jwsactor();
  std::string read();
  void send_msg(const std::string);
  unsigned int lazy_time();

};

#endif
