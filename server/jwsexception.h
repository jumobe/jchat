// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#ifndef JWSEXCEPTION_H
#define JWSEXCEPTION_H

#include <exception>
#include <string>

class jwsexception: public std::exception
{
  
private:
  std::string msg;
  
public:
  jwsexception(const std::string);
  const char* what() const noexcept;

  static void socket_error(const int, const std::string);
};


#endif
