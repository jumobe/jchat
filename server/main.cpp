// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-
/*
 *
 * g++ -std=gnu++14 -Wall -Wextra main.cpp -o main
 */

#include <jlpp.h>
#include "jwsocket.h"

int main(int argc, char **argv)
{
  if( argc < 2 )
  {
    jlpp::err << "Numero de porta nao informado \n";
    return 1;
  }
  
  jwsocket(std::stoi(argv[1]));
  
  return 0;
}
