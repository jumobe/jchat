// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#ifndef JWSOCKET_H
#define JWSOCKET_H

#include "jwsactor.h"

#include <map>

#include <netinet/in.h>

class jwsocket
{

private:
  
  static const unsigned int buffsz = 2048;
  static const unsigned short max_sec_lazy_fd = 22;
  
  unsigned int port;
  int sockfd;
  std::map<unsigned int, jwsactor> allfds;
  struct sockaddr_in serv_addr;  
  char buff[buffsz];
  fd_set rfds;
  unsigned int n_rfds;
  
  void jwslisten();
  void readfds();
  void check_fds(const int);
  void broadcast(std::string) const;
  void close_fds();
  void verify_lazy();
  void close_fd(const unsigned int);
  bool is_main_fd(const unsigned int) const;
  
public:
  explicit jwsocket(const unsigned int) noexcept;
  virtual ~jwsocket();
  
};

#endif
