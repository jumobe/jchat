// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#ifndef JWSENCODE
#define JWSENCODE

#include <string>

class jwsencode
{
  
public:
  static const std::string calc_handshake_hash(const std::string);
  static std::string base64_encode(const unsigned char *, size_t);
  static std::string parseMsg(std::string&);
  static std::string encodeMsg(std::string);

};

#endif
