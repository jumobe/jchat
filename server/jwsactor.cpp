// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#include "jwsactor.h"

#include "jwsexception.h"
#include "jwsencode.h"

#include <jlpp.h>
#include <iostream>
#include <regex>
#include <sstream>

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

jwsactor::jwsactor(const int fd)
{
  this->last_event = 0;
   
  this->fd = fd;

  this->handshake();

  jlpp::dbg << "Fim handshake fd=["
            << std::to_string(this->fd)
            << "] \n";
}

jwsactor::jwsactor()
{
  this->last_event = 0;
}

std::string jwsactor::read()
{
  jlpp::dbg << "Iniciando leitura do fd=["
            << std::to_string(this->fd)
            << "] \n";
  char *buff = (char *)malloc(this->buffsz * sizeof(char));
  memset(buff, 0, this->buffsz);
  
  unsigned long n = recv(this->fd, buff, this->buffsz, 0);

  n = n & 0xfff;

  jlpp::dbg << "read ["
            << std::to_string(n)
            << "] bytes \n";

  this->buffer = ((n > 0) && (n <= this->buffsz))
    ? std::string(buff) : std::string("");
  
  this->buffer.erase(this->buffer.begin() + n, this->buffer.end());

  free(buff);

  this->last_event = 0;
  
  return this->buffer;
}


void jwsactor::handshake()
{
  std::smatch key_match;
  std::regex key_regex ("(Sec-WebSocket-Key: )([^\r]*)");

  auto buff = this->read();

  if( std::regex_search(buff, key_match, key_regex) )
  {
    if(key_match.size() == 3)
    {      
      const std::string enc_key = jwsencode::calc_handshake_hash(key_match[2]);

      std::stringstream resp;

      resp << "HTTP/1.1 101 Switching Protocols"  << "\r\n"
           << "Upgrade: websocket"                << "\r\n"
           << "Connection: Upgrade"               << "\r\n"
           << "Sec-WebSocket-Accept: " << enc_key << "\r\n\r\n";

      jlpp::dbg << resp.str() << "\n";

      this->send_msg(resp.str());
    }
    else
    {
      close(this->fd);
      throw jwsexception("N�o foi possivel obter a chave para handshake");
    }
  }
}

void jwsactor::send_msg(const std::string msg)
{
  send(this->fd, msg.c_str(), msg.length(), 0);
}

unsigned int jwsactor::lazy_time()
{
  return this->last_event++;
}
